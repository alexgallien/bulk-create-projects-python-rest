#!/usr/bin/env python

import getpass
import sys
import json
import requests
import argparse

# -----------------------------------------------------------------------------
'''
Usage

- Creates $num_proj new projects in the target JIRA instance.
- Currently creates business projects, but this can be changed by editing projectTypeKey 
and projectTemplateKey in JSON below.
- Utilizes the undocumented /rest/project-templates/1.0/createshared/ endpoint to create
projects with a shared configuration, to avoid creating a ton of different schemes, etc. 
'''

url = 'http://172.16.56.149:8750'
pkey = 'TEST'
username = 'adminlocal'
password = 'pac1u9drogs4ib'
num_proj = 10

# -----------------------------------------------------------------------------


class ProjectCreator(object):
    def __init__(self, url, pkey, username, password, num_proj):

        self.url = url
        self.pkey = pkey
        self.username = username
        self.password = password
        self.num_proj = num_proj

    def write_data(self, num_proj):

        # Check to see if Project Key passed is valid
        pkey_test = requests.get(
            url + "/rest/api/2/project/" + pkey,
            auth=(self.username, self.password)
        )

        pkey_test_sts = pkey_test.status_code

        # Error handling for most common issues.
        if pkey_test_sts == 200:
            sys.exit("Project Key already exists, choose a unique project key.")
        elif pkey_test_sts == 401:
            sys.exit("Invalid credentials provided, please check username/password")

        # Create the first project
        data = {
            "key": self.pkey,
            "name": "Project " + self.pkey + " Number 1",
            "projectTypeKey": "business",
            "projectTemplateKey": "com.atlassian.jira-core-project-templates:jira-core-project-management",
            "description": "Project Number 1 created by JPP.",
            "lead": self.username
            }

        data = json.dumps(data)

        endpoint = self.url + '/rest/api/2/project'

        r = requests.post(
            endpoint,
            data=data,
            auth=(self.username, self.password),
            headers={'Content-Type': 'application/json'}
        )

        json_content = r.json()

        first_proj_id = json_content['id']

        # Creating the rest of the projects with a shared configuration based off first project.
        if self.num_proj > 1:
            for i in range(2, self.num_proj + 1):

                data = {
                    "key": self.pkey + str(i),
                    "name": "Project " + self.pkey + " Number " + str(i),
                    "projectTypeKey": "business",
                    "projectTemplateKey": "com.atlassian.jira-core-project-templates:jira-core-project-management",
                    "description": "Project Number 1 created by JPP.",
                    "lead": self.username
                }

                data = json.dumps(data)

                endpoint = self.url + '/rest/project-templates/1.0/createshared/' + str(first_proj_id)

                r = requests.post(
                    endpoint,
                    data=data,
                    auth=(self.username, self.password),
                    headers={'Content-Type': 'application/json'}
                )

    def main(self):

        self.write_data(iter)


if __name__ == "__main__":

    # Flag in case you don't want to send pw in plaintext
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-p",
        "--man_pass",
        action='store_true',
        help="Option to securely enter password in the terminal, if you don't want to store in text.")

    options = parser.parse_args()

    if options.man_pass:
        password = getpass.getpass()

    foo = ProjectCreator(
        url=url,
        pkey=pkey,
        username=username,
        password=password,
        num_proj=num_proj
    )

    foo.main()
