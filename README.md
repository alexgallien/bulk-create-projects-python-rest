### Jira Bulk Create Projects ###

A Python script to quickly create a number of projects. Utilizes the undocumented /rest/project-templates/1.0/createshared/ endpoint to create
projects with a shared configuration, to avoid creating a ton of different schemes, etc.

### How do I get set up? ###

- Install Python 2.7 (does not currently support Python 3.x)
- [Install the Requests package](http://docs.python-requests.org/en/master/user/install/)

### How do I use the script? ###

Creates a predefined number of Projects in a Jira instance based on the variables below.

    url = URL of the instance you are hitting, REST endpoint passed automatically
    pkey = Project Key issues will be created as
    username = Username to run REST call with
    password = Password of above
    num_proj = Number of projects to create
        
If plaintext password entry is not desired, use -p to enter password securely.